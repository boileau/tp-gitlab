FROM continuumio/anaconda:4.3.1
MAINTAINER quanteek

COPY . /
RUN conda create -y -n django && \
    /bin/bash -c "source activate django" && \
    conda install -y django

